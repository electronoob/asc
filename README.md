asc
===
Assembly Script

I wanted my own scripting language which I could
convert to bytecode and then run it on my 
microcontroller projects in the future.

The syntax of the language will appear to be a
variant of an assembly language but with some
C-like features to simplify things a little.

I've written down some notes on the current
progress in asc.SPEC and will try to update it
as I go along.

Essentially I'm just throwing ideas into node.js
to see what sticks really, too early to tell
anything more.

